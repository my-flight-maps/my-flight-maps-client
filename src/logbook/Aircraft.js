import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useHistory, useParams } from 'react-router-dom';
import { useAuth } from '../auth/AuthProvider';
import Axios from '../axios';
import AircraftEdit from './AircraftEdit';

const Aircraft = () => {
  const [aircraft, setAircraft] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [shouldRefresh, setShouldRefresh] = useState(false);

  const { checkAuth, token, userId } = useAuth();
  const { registration } = useParams();
  const history = useHistory();

  // Check if the JWT token is stored, then redirect to login if it isn't
  useEffect(() => {
    const fetchAuth = async () => {
      const isAuth = await checkAuth();

      if (!isAuth) {
        history.push('/login');
      }
    }

    fetchAuth();
  }, [checkAuth, history]);

  // Load the aircraft's information
  useEffect(() => {
    const fetchAircraft = async () => {
      const config = {
        headers: {
          'x-auth-token': token
        }
      }

      try {
        const res = await Axios.get(`/api/aircraft/${userId}/${registration}`, config);

        const aircraft = res.data;
        setAircraft(aircraft);
      } catch (err) {
        console.error(err);
      }
    }

    if (registration && userId) {
      fetchAircraft();
      setShouldRefresh(false);
    }
  }, [registration, token, userId, shouldRefresh]);

  // Delete this aircraft
  const onClickDelete = async () => {
    const confirmed = window.confirm(`\
      Are you sure you want to delete your aircraft \
      wtih registration ${aircraft.registration} \
    `.replace(/\s+/gm, ' '));

    if (confirmed) {
      try {
        const res = await Axios.delete(`/api/aircraft/${aircraft._id}`, {
          headers: {
            'x-auth-token': token,
          },
          data: {
            'user_id': userId,
          }
        });

        if (res.status === 200) {
          history.push('/aircraft');
        }
      } catch (err) {
        console.error(err);
      }
    }
  }

  // Switch this page to edit mode
  const onClickEdit = async () => {
    setIsEditing(true);
  }

  return (
    isEditing ?
      <AircraftEdit
        aircraft={ aircraft }
        setIsEditing={ setIsEditing }
        setShouldRefresh={ setShouldRefresh }
      /> :

      <Container>
        { aircraft ?
          <>
            <p><b>Registration: </b> { aircraft.registration.toUpperCase() }</p>
            <p><b>Airline: </b> { aircraft.airline ? aircraft.airline.toUpperCase() : null }</p>
            <p><b>ICAO Type Designator: </b> { aircraft.icao_code ? aircraft.icao_code.toUpperCase() : null }</p>
          </>
          : null
        }
        <Button variant="secondary" onClick={ onClickEdit }>Edit this flight</Button>
        <Button variant="danger" onClick={ onClickDelete }>Delete this flight</Button>
      </Container>
  );
}

export default Aircraft;
