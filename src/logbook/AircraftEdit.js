import Axios from '../axios';
import React, { useState } from 'react';
import { useAuth } from '../auth/AuthProvider';
import { useHistory } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

const AircraftEdit = (props) => {
  const [newReg, setNewReg] = useState(props.aircraft.registration.toUpperCase());
  const [newAirline, setNewAirline] = useState(props.aircraft.airline ?
    props.aircraft.airline.toUpperCase()
    : ''
  );
  const [newIcaoCode, setNewIcaoCode] = useState(props.aircraft.icao_code ?
    props.aircraft.icao_code.toUpperCase()
    : ''
  );

  const { token, userId } = useAuth();

  const history = useHistory();

  // Edit the current flight
  const onClickSaveEdits = async () => {
    const config = {
      headers: {
        'x-auth-token': token,
      }
    }

    const body = {
      "user_id": userId,
      "old_reg": props.aircraft.registration,
      "aircraft": {
        "registration": newReg.toLowerCase(),
        "airline": newAirline.toLowerCase(),
        "icao_code": newIcaoCode.toLowerCase(),
      },
    }

    try {
      const res = await Axios.put(`/api/aircraft/${props.aircraft._id}`, body, config);

      if (res.status === 200) {
        props.setShouldRefresh(true);
        props.setIsEditing(false);
        history.push(`/aircraft/${userId}/${newReg.toLowerCase()}`);
      }
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <Container>
      <Form>
        <Form.Group controlId="Registration">
          <Form.Label>Registration*</Form.Label>
          <Form.Control
            required
            placeholder="Registration"
            value={ newReg }
            onChange={ (e) => setNewReg(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="Airline">
          <Form.Label>Airline ICAO</Form.Label>
          <Form.Control
            placeholder="Airline"
            value={ newAirline }
            onChange={ (e) => setNewAirline(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="icaoCode">
          <Form.Label>ICAO Type Designator</Form.Label>
          <Form.Control
            placeholder="ICAO Type Designator"
            value={ newIcaoCode }
            onChange={ (e) => setNewIcaoCode(e.target.value) }
          />
        </Form.Group>

        <Form.Text>* required fieleds</Form.Text>
        <Button variant="outline-secondary" onClick={ () => props.setIsEditing(false) }>
          Cancel Edits
        </Button>
        <Button variant="outline-primary" onClick={ onClickSaveEdits }>
          Edit Aircraft
        </Button>
      </Form>
    </Container>
  );
};

export default AircraftEdit;
