import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../auth/AuthProvider';
import Axios from '../axios';

const AircraftList = () => {
  const [registration, setRegistration] = useState('');
  const [airline, setAirline] = useState('');
  const [icaoCode, setIcaoCode] = useState('');
  const [aircraft, setAircraft] = useState([]);
  const [shouldRefresh, setShouldRefresh] = useState(false);

  const { checkAuth, token, userId } = useAuth();

  const history = useHistory();

  // Check if the JWT token is stored, then redirect to login if it isn't
  useEffect(() => {
    const fetchAuth = async () => {
      const isAuth = await checkAuth();

      if (!isAuth) {
        history.push('/login');
      }
    }

    fetchAuth();
  }, [checkAuth, history]);

  // Load all of a user's flights
  useEffect(() => {
    const fetchFlights = async () => {
      const config = {
        headers: {
          'x-auth-token': token,
        }
      }

      try {
        const res = await Axios.get(`/api/aircraft/${userId}`, config,);

        setAircraft(res.data);
      } catch (err) {
        console.error(err);
      }
    }

    if (token && userId) {
      fetchFlights();
      setShouldRefresh(false);
    }
  }, [token, userId, shouldRefresh]);

  // Add a new aircraft
  const onClickAddAircraft = async () => {
    const config = {
      headers: {
        'x-auth-token': token,
      }
    }

    const body = {
      "user_id": userId,
      "registration": registration.toLowerCase(),
      "airline": airline.toLowerCase(),
      "icao_code": icaoCode.toLowerCase()
    }

    try {
      const res = await Axios.post(`/api/aircraft/`, body, config);

      if (res.status === 200) {
        setRegistration('');
        setAirline('');
        setIcaoCode('');

        setShouldRefresh(true);
      }
    } catch (err) {
      console.error(err);
    }
  }

  // View a specific aircraft's page
  const onClickView = (aircraft) => {
    history.push(`/aircraft/${userId}/${aircraft.registration}`);
  }

  // Helper to map the aircraft list to <tr>s for the Table
  const generateTable = aircraft.map((aircraft, key) => (
    <tr key={ key }>
      <td>{ aircraft.registration.toUpperCase() }</td>
      <td>{ aircraft.airline ? aircraft.airline.toUpperCase() : null }</td>
      <td>{ aircraft.icao_code ? aircraft.icao_code.toUpperCase() : null }</td>
      <td>
        <Button variant="outline-primary" onClick={ () => onClickView(aircraft) }>
          View
        </Button>
      </td>
    </tr>
  ));

  return (
    <Container >
      <h1>Aircraft List</h1>

      <Form>
        <Form.Group controlId="Registration">
          <Form.Label>Registration*</Form.Label>
          <Form.Control
            placeholder="Registration"
            value={ registration }
            onChange={ (e) => setRegistration(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="Airline">
          <Form.Label>Airline ICAO</Form.Label>
          <Form.Control
            placeholder="Airline"
            value={ airline }
            onChange={ (e) => setAirline(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="icaoCode">
          <Form.Label>ICAO Type Designator</Form.Label>
          <Form.Control
            placeholder="ICAO Type Designator"
            value={ icaoCode }
            onChange={ (e) => setIcaoCode(e.target.value) }
          />
        </Form.Group>

        <Form.Text>* required fieleds</Form.Text>
        <Button variant="primary" onClick={ onClickAddAircraft }>
          Add Aircraft
        </Button>
      </Form>

      <Table responsive striped bordered hover style={ { marginTop: '15px' } } >
        <thead>
          <tr>
            <th>Registration</th>
            <th>Airline ICAO Code</th>
            <th>ICAO Type Designator</th>
            <th />
          </tr>
        </thead>
        <tbody>
          { generateTable }
        </tbody>
      </Table>
    </Container>
  );

};

export default AircraftList;
