import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../auth/AuthProvider';
import Axios from '../axios';

const Logbook = () => {
  const [departureIcao, setDepartureIcao] = useState('');
  const [arrivalIcao, setArrivalIcao] = useState('');
  const [flightNum, setFlightNum] = useState('');
  const [callsign, setCallsign] = useState('');
  const [registration, setRegistration] = useState('');
  const [flights, setFlights] = useState([]);
  const [shouldRefresh, setShouldRefresh] = useState(false);

  const { checkAuth, token, userId } = useAuth();

  const history = useHistory();

  // Check if the JWT token is stored, then redirect to login if it isn't
  useEffect(() => {
    const fetchAuth = async () => {
      const isAuth = await checkAuth();

      if (!isAuth) {
        history.push('/login');
      }
    }

    fetchAuth();
  }, [checkAuth, history]);

  // Load all of a user's flights
  useEffect(() => {
    const fetchFlights = async () => {
      const config = {
        headers: {
          'x-auth-token': token,
        }
      }

      try {
        const res = await Axios.get(`/api/flights/${userId}`, config,);

        setFlights(res.data);
      } catch (err) {
        console.error(err);
      }
    }

    if (token && userId) {
      fetchFlights();
      setShouldRefresh(false);
    }
  }, [token, userId, shouldRefresh]);

  // Add a new flight to the logbook
  const onClickAddFlight = async () => {
    const config = {
      headers: {
        'x-auth-token': token,
      }
    }

    const body = {
      "user_id": userId,
      "departure_icao": departureIcao.toLowerCase(),
      "arrival_icao": arrivalIcao.toLowerCase(),
      "flight_number": flightNum.toLowerCase(),
      "callsign": callsign.toLowerCase(),
      "registration": registration.toLowerCase(),
    }

    try {
      const res = await Axios.post(`/api/flights/`, body, config);

      if (res.status === 200) {
        setDepartureIcao('');
        setArrivalIcao('');
        setFlightNum('');
        setCallsign('');
        setRegistration('');

        setShouldRefresh(true);
      }
    } catch (err) {
      console.error(err);
    }
  }

  const onClickView = (flight) => {
    history.push({
      pathname: `/flight/${flight._id}`,
      state: { flight: flight }
    });
  }

  // Helper to map flights list to <tr>s
  const generateTable = flights.map((flight, key) => (
    <tr key={ key }>
      <td>{ flight.departure_icao.toUpperCase() }</td>
      <td>{ flight.arrival_icao.toUpperCase() }</td>
      <td>{ flight.callsign ? flight.callsign.toUpperCase() : null }</td>
      <td>{ flight.flight_number ? flight.flight_number.toUpperCase() : null }</td>
      <td>{ flight.registration ? flight.registration.toUpperCase() : null }</td>
      <td>{ new Date(flight.date).toDateString() }</td>
      <td>
        <Button variant="outline-primary" onClick={ () => onClickView(flight) }>
          View
        </Button>
      </td>
    </tr>
  ));

  return (
    <Container >
      <h1>Logbook</h1>

      <Form>
        <Form.Group controlId="departureIcao">
          <Form.Label>Departure airport ICAO* </Form.Label>
          <Form.Control
            placeholder="Departure ICAO"
            value={ departureIcao }
            onChange={ (e) => setDepartureIcao(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="arrivalIcao">
          <Form.Label>Arrival airport ICAO* </Form.Label>
          <Form.Control
            placeholder="Arrival ICAO"
            value={ arrivalIcao }
            onChange={ (e) => setArrivalIcao(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="flightNumber">
          <Form.Label>Flight Number</Form.Label>
          <Form.Control
            placeholder="Flight number"
            value={ flightNum }
            onChange={ (e) => setFlightNum(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="callsign">
          <Form.Label>Callsign</Form.Label>
          <Form.Control
            placeholder="Callsign"
            value={ callsign }
            onChange={ (e) => setCallsign(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="registration">
          <Form.Label>Aircraft registration</Form.Label>
          <Form.Control
            placeholder="Registration"
            value={ registration }
            onChange={ (e) => setRegistration(e.target.value) }
          />
        </Form.Group>
        <Form.Text>* required fields</Form.Text>

        <Button variant="primary" onClick={ onClickAddFlight }>
          Add Flight
        </Button>
      </Form>

      <Table responsive striped bordered hover style={ { marginTop: '15px' } } >
        <thead>
          <tr>
            <th>Departed</th>
            <th>Arrived at</th>
            <th>Callsign</th>
            <th>Flight #</th>
            <th>Aircraft reg.</th>
            <th>Date</th>
            <th />
          </tr>
        </thead>
        <tbody>
          { generateTable }
        </tbody>
      </Table>
    </Container>
  );

};

export default Logbook;
