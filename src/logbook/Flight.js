import Axios from '../axios';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useAuth } from '../auth/AuthProvider';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

const Flight = (props) => {

  const { checkAuth, token, userId } = useAuth();
  const { flightId } = useParams();
  const history = useHistory();

  // Check if the JWT token is stored, then redirect to login if it isn't
  useEffect(() => {
    const fetchAuth = async () => {
      const isAuth = await checkAuth();

      if (!isAuth) {
        history.push('/login');
      }
    }

    fetchAuth();
  }, [checkAuth, history]);

  // Fetch the current flight if we navigated here on our own
  const fetchFlight = async () => {
    const config = {
      headers: {
        'x-auth-token': token,
      }
    }

    try {
      const res = await Axios.get(`/api/flights/${userId}/${flightId}`, config);

      return res.data;
    } catch (err) {
      console.error(err);
    }
  }

  // Set flight so we don't need to check both everywhere
  const flight = props.location.state ?
    props.location.state.flight :
    fetchFlight();

  const [isEditing, setIsEditing] = useState(false);
  const [departureIcao, setDepartureIcao] = useState(flight.departure_icao.toUpperCase());
  const [arrivalIcao, setArrivalIcao] = useState(flight.arrival_icao.toUpperCase());
  const [callsign, setCallsign] = useState(flight.callsign ? flight.callsign.toUpperCase() : '');
  const [flightNum, setFlightNum] = useState(flight.flight_number ? flight.flight_number.toUpperCase() : '');
  const [registration, setRegistration] = useState(flight.registration ? flight.registration.toUpperCase() : '');

  // Delete this flight
  const onClickDelete = async () => {
    const confirmed = window.confirm(`\
      Are you sure you want to delete your flight \
      from ${flight.departure_icao.toUpperCase()} \
      to ${flight.arrival_icao.toUpperCase()} \
      on ${new Date(flight.date).toDateString()}?
    `.replace(/\s+/gm, ' '));

    if (confirmed) {
      try {
        const res = await Axios.delete(`/api/flights/${flight._id}`, {
          headers: {
            'x-auth-token': token,
          },
          data: {
            'user_id': userId,
          }
        });

        if (res.status === 200) {
          history.push('/logbook');
        }
      } catch (err) {
        console.error(err);
      }
    }
  }

  // Switch this page to edit mode
  const onClickEdit = async () => {
    setIsEditing(true);
  }

  // Edit the current flight
  const onClickSaveEdits = async () => {
    const config = {
      headers: {
        'x-auth-token': token,
      }
    }

    const body = {
      "user_id": userId,
      "flight": {
        "departure_icao": departureIcao.toLowerCase(),
        "arrival_icao": arrivalIcao.toLowerCase(),
        "flight_number": flightNum.toLowerCase(),
        "callsign": callsign.toLowerCase(),
        "registration": registration.toLowerCase(),
      }
    }

    try {
      const res = await Axios.put(`/api/flights/${flight._id}`, body, config);

      if (res.status === 200) {
        setIsEditing(false);
        history.push('/logbook');
      }
    } catch (err) {
      console.error(err);
    }
  }

  // View this aircraft's page
  const onClickViewAircraft = async () => {
    history.push(`/aircraft/${userId}/${flight.registration}`);
  }

  if (isEditing) {
    return (
      <Container>
        <Form>
          <Form.Group controlId="departureIcao">
            <Form.Label>Departure airport ICAO* </Form.Label>
            <Form.Control
              placeholder="Departure ICAO"
              value={ departureIcao }
              onChange={ (e) => setDepartureIcao(e.target.value) }
            />
          </Form.Group>

          <Form.Group controlId="arrivalIcao">
            <Form.Label>Arrival airport ICAO* </Form.Label>
            <Form.Control
              placeholder="Arrival ICAO"
              value={ arrivalIcao }
              onChange={ (e) => setArrivalIcao(e.target.value) }
            />
          </Form.Group>

          <Form.Group controlId="flightNumber">
            <Form.Label>Flight Number</Form.Label>
            <Form.Control
              placeholder="Flight number"
              value={ flightNum }
              onChange={ (e) => setFlightNum(e.target.value) }
            />
          </Form.Group>

          <Form.Group controlId="callsign">
            <Form.Label>Callsign</Form.Label>
            <Form.Control
              placeholder="Callsign"
              value={ callsign }
              onChange={ (e) => setCallsign(e.target.value) }
            />
          </Form.Group>

          <Form.Group controlId="registration">
            <Form.Label>Aircraft registration</Form.Label>
            <Form.Control
              placeholder="Registration"
              value={ registration }
              onChange={ (e) => setRegistration(e.target.value) }
            />
          </Form.Group>
          <Form.Text>* required fieleds</Form.Text>

          <Button variant="primary" onClick={ onClickSaveEdits }>
            Save Edits
        </Button>
        </Form>
      </Container>
    );
  }
  else {
    return (
      <Container>
        <p>
          Flight from <b>{ flight.departure_icao.toUpperCase() }</b>{ ' ' }
          to <b>{ flight.arrival_icao.toUpperCase() }</b>{ ' ' }
          on <b>{ new Date(flight.date).toDateString() }</b>
        </p>

        <p><b>Departure ICAO:</b> { flight.departure_icao.toUpperCase() }</p>
        <p><b>Arrival ICAO:</b> { flight.arrival_icao.toUpperCase() }</p>
        <p><b>Callsign:</b> { flight.callsign ? flight.callsign.toUpperCase() : null }</p>
        <p><b>Flight number:</b> { flight.flight_number ? flight.flight_number.toUpperCase() : null }</p>
        <p><b>Registration:</b> { flight.registration ? flight.registration.toUpperCase() : null }</p>

        <Button variant="secondary" onClick={ onClickEdit }>Edit this flight</Button>
        <Button variant="danger" onClick={ onClickDelete }>Delete this flight</Button>
        <Button variant="secondary" onClick={ onClickViewAircraft }>View this aircraft</Button>
      </Container >
    );
  }
}

export default Flight;
