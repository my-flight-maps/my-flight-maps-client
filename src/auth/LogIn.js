import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useHistory } from 'react-router-dom';
import { useAuth } from './AuthProvider';

function LogIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { logIn } = useAuth();

  const history = useHistory();

  async function onClickLogIn() {
    try {
      await logIn(email, password);
      history.push('/');
    } catch (err) {
      console.error(err.message);
    }
  }

  return (
    <Container>
      <h1>Log in</h1>
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={ email }
            onChange={ (e) => setEmail(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={ password }
            onChange={ (e) => setPassword(e.target.value) }
          />
        </Form.Group>
        <Button variant="primary" onClick={ onClickLogIn }>
          Log in
        </Button>
      </Form>
      <Button variant="secondary" onClick={ () => history.push('/signup') } style={ { marginTop: 20 } }>
        Need an account?
      </Button>
    </Container>
  );
}

export default LogIn;
