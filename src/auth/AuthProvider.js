import Axios from '../axios';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';

// Create a new Context object that will be provided to descendants of
// the AuthProvider.
const AuthContext = React.createContext(null);

/** 
 * The AuthProvider is responsible for user management and provides the
 * AuthContext value to its descendants. Components under an AuthProvider can
 * use the useAuth() hook to access the auth value.
 */
const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(null);
  const [userId, setUserId] = useState(null);
  const history = useHistory();

  /** 
   * The checkAuth function is used when a user
   * navigates to a page requiring authorization without being logged in
   * if the user has a token, this will authorize them.
   */
  const checkAuth = async () => {
    if (token && userId) {
      return true;
    }

    try {
      const token = localStorage.getItem('auth-token');

      const res = await Axios.post(`/api/users/checkauth`, { token });

      if (!res.data.user) {
        return false;
      }

      setUserId(res.data.user._id);
      setToken(token);

      return true;
    } catch (err) {
      return false;
    }
  };

  /** The logIn function takes an email and password to log the user in */
  const logIn = async (email, password) => {
    try {
      const user = { email, password };

      const res = await Axios.post(`/api/users/login`, { user });

      const token = res.data.user.token;
      localStorage.setItem('auth-token', token);

      setUserId(res.data.user._id);
      setToken(token);

      history.push('/');
    } catch (err) {
      console.error(err);
    }
  };

  /** The signUp function takes an email and password to sign the user up */
  const signUp = async (email, password) => {
    try {
      const newUser = { email, password };

      await Axios.post(`/api/users/`, { "user": newUser });

      history.push('/login');

    } catch (err) {
      console.log(err);
    }
  };

  /** 
   * The logOut functions signs the currently logged-in user out 
   * 
   * It also clears all state variables so that a bad-actor cannot 
   * use them after the user logs out
   */
  const logOut = async () => {
    if (userId == null) {
      console.warn("Not logged in, can't log out!");
      return;
    }

    localStorage.setItem('auth-token', undefined);
    setToken(null);
    setUserId(null);
  };

  return (
    <AuthContext.Provider
      value={ {
        checkAuth,
        logIn,
        logOut,
        signUp,
        token,
        userId,
      } }
    >
      {children }
    </AuthContext.Provider>
  );
};

/** Create a context for AuthProvider's children to use */
const useAuth = () => {
  const auth = useContext(AuthContext);
  if (auth == null) {
    throw new Error('useAuth() called outside of an AuthProvider?');
  }
  return auth;
};

export default AuthProvider;
export { useAuth };

