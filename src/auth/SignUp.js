import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useHistory } from 'react-router-dom';
import { useAuth } from './AuthProvider';

function SignUp() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');

  const { signUp } = useAuth();

  const history = useHistory();

  async function onClickSignUp() {
    if (password !== verifyPassword) {
      window.alert('Your passwords must match!');
    }

    try {
      await signUp(email, password);
    } catch (err) {
      console.error(err.message);
    }
  }

  return (
    <Container>
      <h1>Sign up</h1>
      <Form>
        <Form.Group controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={ email }
            onChange={ (e) => setEmail(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={ password }
            onChange={ (e) => setPassword(e.target.value) }
          />
        </Form.Group>

        <Form.Group controlId="verifyPassword">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Re-type password"
            value={ verifyPassword }
            onChange={ (e) => setVerifyPassword(e.target.value) }
          />
        </Form.Group>

        <Button variant="primary" onClick={ onClickSignUp }>
          Sign Up
        </Button>

      </Form>
      <Button variant="secondary" onClick={ () => history.push('/login') } style={ { marginTop: 20 } }>
        Already have an account?
      </Button>
    </Container>
  );
}

export default SignUp;
