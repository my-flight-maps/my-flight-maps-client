import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../auth/AuthProvider';
import Button from 'react-bootstrap/Button';

const Home = () => {
  const { checkAuth, logOut } = useAuth();
  const history = useHistory();

  // Check if the JWT token is stored, then redirect to login if it isn't
  useEffect(() => {
    const fetchAuth = async () => {
      const isAuth = await checkAuth();

      if (!isAuth) {
        history.push('/login');
      }
    }

    fetchAuth();
  }, [checkAuth, history]);

  // Used to log the user out
  const onClickLogOut = () => {
    logOut();
  };


  return (
    <div className='container'>
      <h1>Home</h1>
      <p>Welcome to My Flight Maps!</p>
      <p>
        This site is a personal project and in very early beta, so please be patient with any breaking issues.
      </p>
      <Button
        variant="danger"
        onClick={ onClickLogOut }
      >
        Log out
      </Button>
    </div>
  );

};

export default Home;
