import Axios from 'axios';

const instance = Axios.create({
  baseURL: 'https://api.easysimlog.com'
});


export default instance;
