import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import './App.css';
import AuthProvider from './auth/AuthProvider';
import LogIn from './auth/LogIn';
import SignUp from './auth/SignUp';
import Home from './home/Home';
import Aircraft from './logbook/Aircraft';
import AircraftList from './logbook/AircraftList';
import Flight from './logbook/Flight';
import Logbook from './logbook/Logbook';

function App() {
  return (
    <BrowserRouter>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>Easy Sim Log</Navbar.Brand>
        <Nav className="mr-auto" onSelect={ (eventKey, _) => console.log(eventKey) }>
          <Nav.Link as={ Link } to="/">Home</Nav.Link>
          <Nav.Link as={ Link } to="/logbook">Logbook</Nav.Link>
          <Nav.Link as={ Link } to="/aircraft">Aircraft List</Nav.Link>
        </Nav>
      </Navbar>

      <AuthProvider>
        <Switch>
          <Route exact path='/' component={ Home } />
          <Route exact path='/login' component={ LogIn } />
          <Route exact path='/signup' component={ SignUp } />
          <Route exact path='/aircraft' component={ AircraftList } />
          <Route path='/logbook' component={ Logbook } />
          <Route path='/flight/:flightId' component={ Flight } />
          <Route path='/aircraft/:userId/:registration' component={ Aircraft } />
        </Switch>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
